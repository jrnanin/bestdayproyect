/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');

exports.createToken= function(user){
    var payload =  {
        sub:user.idUser,
        name:user.name,
        apaterno:user.apaterno,
        amaterno:user.amaterno,
        role:user.role,
        puesto:user.puesto,
        canal:user.canal,
        iat:moment().unix(),
        exp:moment().add(30,'days').unix(),
        status:user.status
    };
    //console.log(payload);
    return jwt.encode(payload,config.SECRET_TOKEN);
};