/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */

'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
const config = require('../config');
var payload = "";

//Mejorar el funcionamiento del token para los casos de
exports.ensureAuth = function (req, res, next) {
    if (!req.headers.authorization) {
        res.status(500).json({mensaje: "no se tiene la cabecera authentication"});
    } else {
        var token = req.headers.authorization.replace(/['"]+/g, '');
        try {
            payload = jwt.decode(token, config.SECRET_TOKEN);
            req.user = payload;
            next();
        }
        catch (ex) {
            console.log(ex);
            res.status(500).json({mensaje: "Token expired"});
        }
    }

};