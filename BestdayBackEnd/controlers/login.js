/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const Users = require('.././models/userModel');
const Role = require('.././models/roleModel');
var jwt = require('../services/jwt');
const crypto = require('crypto');
const global = require('../global/global');
const secret = 'abcdefg';

function login(req, res) {
    var data = req.body;
    console.log(data);
    if (data.correo == undefined || data.password == undefined) {
        res.status(500).json({mensaje: "Faltan datos", code: global.GLOBAL.E501});
    } else if (data.correo.length == 0 || data.password.length == 0) {
        res.status(500).json({mensaje: "Faltan datos", code: global.GLOBAL.E501});
    }
    else {
        Users.findOne({correo: data.correo, delete: false})
            .populate({path: "puesto", select: 'name'})
            .populate({path: "role", select: 'name'})
            .populate({path: "canal", select: 'name'})
            .exec(function (err, user) {
                    if (err) {
                        console.log("error para insertar -> " + err);
                        res.status(500).json({mensaje: "error inesperado"});
                    }
                    else if (user == null) {
                        console.log("Usuario con correo " + data.correo + " no encontrado");
                        res.status(500).json({mensaje: "Usuario no encontrado", code: global.GLOBAL.E502});
                    }
                    else {
                        console.log(user);
                        var crypUser = crypto.createHmac('sha256', secret).update(data.password).digest('hex');
                        if (crypUser === user.password) {
                            //Si el usuario esta activo se le permite el acceso al sistema
                            if (user.status === true) {
                                //crear token
                                res.status(200).json({
                                    mensaje: "Usuario login",
                                    status: "acept",
                                    code: global.GLOBAL.M200,
                                    "user_token": jwt.createToken(user)
                                });
                            }
                            else {
                                res.status(500).json({mensaje: "Usuario no permitido", code: global.GLOBAL.E503});
                            }
                        }
                        else {
                            res.status(500).json({mensaje: "No coinciden los datos", code: global.GLOBAL.E504});
                        }
                    }


                }
            );
    }
}

module.exports = {
    login
};