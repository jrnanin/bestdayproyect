/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const Canal = require('../models/canalModel');

function newcanal(req, res) {
    data = req.body;
    console.log(data);
    var canal = new Canal();
    canal.name = data.name;
    canal.delete = false;
    canal.save(function (err, canal) {
        if (err) {
            console.log("Error al salvar el nuevo canal: " + err + "\n");
            return res.status(500).json({mensaje: "Error al salvar el nuevo canal: " + err});
        }
        else {
            return res.status(200).json({mensaje: "Registro Mensaje OK", data: canal});
        }

    });

}
function getcanales(req, res) {
    Canal.find({ delete: false},function (error, canales) {
        if (error) {
            console.log('Tenemos algun erro al buscar los canales');
            return res.status(500).json({mensaje: 'TTenemos algun erro al buscar los canales',error});
        } else {
            return res.status(200).json({mensaje: "Canales Ok", data:canales});
        }
    });
}

module.exports = {
    newcanal,
    getcanales
};