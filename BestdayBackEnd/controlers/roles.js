/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const Role = require('.././models/roleModel');

function newRole(req, res) {

    data = req.body;
    var role = new Role();
    role.name = data.nombre;
    role.delete = false;

    role.save(function (err, role) {
        if (err) {
            console.log("Error crear nuevo Role: " + err + "\n");
            res.status(500).json({mensaje: "Error Role ya usado"});
        }
        else {
            res.status(200).json({mensaje: "Registro Role OK"});
        }
    });

}
function getRoles(req, res){
    Role.find({delete: false},function(err,roles){
        if(err){
            console.log("No Existen Roles: " + err + "\n");
            res.status(500).json({mensaje: "No Existen Roles"});
        }
        return res.status(200).json({mensaje: "Roles Ok", data: roles});

    });
}

module.exports = {
    newRole,
    getRoles

};