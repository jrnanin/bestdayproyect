/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const Puesto = require('../models/puestoModel');

function newPuesto(req, res) {
    data = req.body;
    var puesto = new Puesto();
    puesto.name = data.name;
    puesto.delete=false;
    puesto.save(function (error, puesto) {
        if (error) {
            console.log("Error al salvar el nuevo puesto: " + error + "\n");
            return res.status(500).json({mensaje: "Error al salvar el nuevo puesto: ", error:error});
        }
        else {
            return res.status(200).json({mensaje: "Registro Puesto OK", data: puesto});
        }
    });

}

function getPuesto(req, res) {
    var puestoid = req.params.idpuesto;
    Puesto.find({_id: puestoid, delete: false}, function (error, puesto) {
        if (error) {
            console.log('Tenemos algun erro al buscar el puesto');
            return res.status(500).json({mensaje: 'Tenemos algun erro al buscar el puesto',error:error});
        } else {
            return res.status(200).json({mensaje: "Puesto Ok", data:puesto});
        }
    });

}

function getPuestos(req, res) {
    Puesto.find({ delete: false}, function (error, puestos) {
        if (error) {
            console.log('Tenemos algun erro al buscar los puestos');
            return res.status(500).json({mensaje: 'Tenemos algun erro al buscar los puestos',error:error});
        } else {
            return res.status(200).json({mensaje: "Puestos Ok", data:puestos});
        }
    });
}
function deletePuesto(req, res) {

}
function updatePuesto(req, res) {

}

module.exports = {
    newPuesto,
    getPuesto,
    getPuestos,
    deletePuesto,
    updatePuesto

}