/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const Proyecto = require('../models/proyectoModel');

function newProyecto(req, res) {
    data = req.body;
    console.log(data);
    var proyecto = new Proyecto();
    proyecto.name = data.name;
    proyecto.anunciante = data.anunciante;
    proyecto.imagen = data.imagen;
    proyecto.status = 'iniciado';
    proyecto.delete = false;
    proyecto.fechaInicio = new Date();
    proyecto.save(function (error, proyecto) {
        if (error) {
            console.log('Tenemos algun erro al crear el proyecto');
            return res.status(500).json({mensaje: 'Tenemos algun erro alcrear el proyecto', error: error});
        } else {
            return res.status(200).json({mensaje: "Proyecto Ok", data: proyecto});
        }
    });


}

function getProyecto(req, res) {
    var idProyecto = req.params.idproyecto;
    Proyecto.findOne({_id: idProyecto, delete: false}, {__v: 0, delete: 0})
        .populate({
            path: "trimestre",
            populate: {
                path: "medio",
                ref: "medio"
            },
            populate: {
                path: "reportes",
                ref: "reporte",
                populate: {
                    path: "medio",
                    populate: {
                        path: "canal",
                        ref: "canal"
                    }
                }
            }
        })
        .populate({path: "medio"})
        .exec(function (error, proyectos) {
            if (error) {
                console.log('Tenemos algun erro al buscar los proyectos');
                return res.status(500).json({mensaje: 'Tenemos algun erro al buscar los proyectos', error: error});
            } else {
                return res.status(200).json({mensaje: "Proyectos Ok", data: proyectos});
            }
        });
}

function getProyectos(req, res) {
    Proyecto.find({delete: false}, function (error, proyectos) {
        if (error) {
            console.log('Tenemos algun erro al buscar los proyectos');
            return res.status(500).json({mensaje: 'Tenemos algun erro al buscar los proyectos', error: error});
        } else {
            return res.status(200).json({mensaje: "Proyectos Ok", data: proyectos});
        }
    });
}

function getProyectosByCliente(req, res) {
}

function updateProyecto(req, res) {
}

function deleteProyecto(req, res) {

}

module.exports = {
    newProyecto,
    getProyecto,
    getProyectos,
    getProyectosByCliente,
    updateProyecto,
    deleteProyecto
}