/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */

const Trimestre = require('../models/trimestreModel');
const Proyecto = require('../models/proyectoModel');
const Reporte = require('../models/reporteModel');

function newTrimestre(req, res) {
    var data = req.body;
    console.log(data.proyecto);

    Proyecto.findOne({_id: data.proyecto, delete: false}, {__v: 0, delete: 0})
        .populate({path: "trimestre", populate: {path: "medio",ref: "medio"}})
        .populate({path: "medio",ref: "medio"})
        .exec(function (error, proyectofind) {
            if (error) {
                console.log('Tenemos algun error al buscar los proyectos');
            }
            else {
                var trimestre = new Trimestre();
                proyectofind.trimestre.push(trimestre.id);
                //console.log(JSON.stringify(proyecto));
                trimestre.fechaCreacion = new Date();
                trimestre.medio = proyectofind.medio;
                for (var ind = 0; ind < proyectofind.medio.length; ind++)
                {
                    var reporte = new Reporte();
                    reporte.medio = proyectofind.medio[ind];
                    reporte.fechaCreacion = new Date();
                    trimestre.reportes.push(reporte._id);
                    reporte.save(function (error, reporte) {
                        if (error) {
                            console.log('Tenemos algun error al guardar el reporte');
                        }
                    });
                }

                trimestre.save(function (error, trimestres) {
                    if (error) {
                        console.log('Tenemos algun error al crear el trimestres');
                        return res.status(500).json({
                            mensaje: 'Tenemos algun error al crear el trimestres',
                            error: error
                        });
                    } else {

                        proyectofind.save(function (error, proyectosave) {
                            if (error) {
                                console.log('Tenemos algun error al guardar los cambios del proyecto');
                            } else {
                                return res.status(200).json({mensaje: "Trimestre Ok", data: trimestre});
                            }
                        });

                    }
                });


            }
        });

}

function getTrimestre(req, res) {
    var idTrimestre = req.params.idTrimestre;
    Trimestre.findOne({_id: idTrimestre, delete: false}, {__v: 0, delete: 0})
        .populate({path: "reportes"})
        .populate({path: "medio"})
        .exec(function (error, trimestre) {
            if (error) {
                console.log('Tenemos algun erro al buscar los proyectos');
                return res.status(500).json({mensaje: 'Tenemos algun erro al buscar los proyectos', error: error});
            } else {
                return res.status(200).json({mensaje: "Proyectos Ok", data: trimestre});
            }
        });
}

function getTrimestres(req, res) {
    Trimestre.find({delete: false})
        .populate({path: "reportes"})
        .populate({path: "medio"})
        .exec(function (error, trimestre) {
            if (error) {
                console.log('Tenemos algun erro al buscar los trimestres');
                return res.status(500).json({mensaje: 'Tenemos algun erro al buscar los trimestres', error: error});
            } else {
                return res.status(200).json({mensaje: "Trimestres Ok", data: trimestre});
            }
        });
}

function getTrimestresByProyecto(req, res) {
}

function updateTrimestre(req, res) {
}

function deleteTrimestre(req, res) {
}


module.exports = {
    newTrimestre,
    getTrimestre,
    getTrimestres,
    getTrimestresByProyecto,
    updateTrimestre,
    deleteTrimestre
}