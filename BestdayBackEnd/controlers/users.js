/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const User = require('.././models/userModel');
const Medio = require('.././models/mediosModel');
const Canal = require('.././models/canalModel');
const crypto = require('crypto');

const secret = 'abcdefg';


function newUser(req, res) {
    //console.log(req.body);
    data = req.body;
    console.log(data);
    var user = new User();
    user.name = data.name;
    user.apaterno = data.apaterno;
    user.amaterno = data.amaterno;
    user.correo = data.correo;
    user.password = crypto.createHmac('sha256', secret).update(data.password).digest('hex');
    user.puesto = JSON.parse(data.puesto);
    user.canal = JSON.parse(data.canal);
    user.medio = JSON.parse(data.medio);
    user.role = JSON.parse(data.role);
    user.dateCreate = new Date();
    user.status = true;
    user.delete = false;

    user.save(function (error, user) {
        if (error) {
            console.log("Error al salvar el nuevo usuario: " + err + "\n");
            return res.status(500).json({mensaje: "Error Correo ya usado", error});
        }
        else {
            return res.status(200).json({mensaje: "Registro Mensaje OK"});
        }
    });

}

function getUser(req, res) {
    var userid = req.params.userid;
    //console.log(userid);
    User.findOne({_id: userid, delete: false}, {
        password: 0,
        dateCreate: 0,
        delete: 0,
        _v: 0,
        idUser: 0
    }, function (error, user) {
        if (error) {
            return res.status(500).json({mensaje: "No existe el usuario"});
        }
        else {
            //console.log(user)
            if (user == null) {
                return res.status(500).json({mensaje: "No existe el usuario", error});
            } else {
                return res.status(200).json({mensaje: "usuario encontrado", data: castUser(user)});
            }
        }
    });
}

function getUsers(req, res) {
    User.find({delete: false}, {password: 0, dateCreate: 0, delete: 0, _v: 0, idUser: 0})
        .populate({path: "medio"})
        .populate({path: "puesto"})
        .populate({path: "canal"})
        .populate({path: "role"})
        .exec(function (error, users) {
            if (error) {
                return res.status(500).json({mensaje: "No existen usuarios", error});
            }
            else {
                //console.log(users.length);

                var arrUsers = [];
                for (var ind = 0; ind < users.length; ind++) {
                    arrUsers.push(castUser(users[ind]));
                    //console.log(users[ind]);
                }
                return res.status(200).json({mensaje: "usuario encontrado", data: arrUsers});
            }
        });
}

function updateUser(req, res) {

}

function deleteUser(req, res) {

}

module.exports = {
    newUser,
    getUsers,
    getUser,
    updateUser,
    deleteUser

};

function castUser(user) {
    //console.log(user);
    var userObj = {
        name: user.name,
        apaterno: user.apaterno,
        amaterno: user.amaterno,
        puesto: user.puesto,
        canal: user.canal,
        medio: user.medio,
        correo: user.correo,
        role: user.role,
        status: user.status,
        idUser: user._id
    };
    return userObj;
}