/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const Medio = require('.././models/mediosModel');
const Canal = require('.././models/canalModel');

function newmedio(req, res) {
    data = req.body;
    var medio = new Medio();
    medio.name = data.name;
    medio.canal = data.canal;
    medio.delete = false;
    medio.save(function (err, medio) {
        if (err) {
            console.log("Error al salvar el nuevo usuario: " + err + "\n");
            return res.status(500).json({mensaje: "Error al salvar el nuevo usuario: " + err});
        }
        else {
            return res.status(200).json({mensaje: "Registro Mensaje OK", data: medio});
        }
    });

}

function getmedio(req, res) {
    var medioid = req.params.idmedio;
    Medio.find({_id: medioid, delete: false}, function (error, medio) {
        if (error) {
            console.log('Tenemos algun erro al buscar el medio');
            return res.status(500).json({mensaje: 'Tenemos algun erro al buscar el medio', error});
        } else {
            return res.status(200).json({mensaje: "Medio Ok", data: medio});
        }
    });

}

function getmedios(req, res) {
    Medio.find({delete: false}, function (error, medio) {
        if (error) {
            console.log('Tenemos algun erro al buscar el medio');
            return res.status(500).json({mensaje: 'Tenemos algun erro al buscar el medio', error});
        } else {
            Canal.populate(medio, {path: "canal"}, function (error, medios) {
                if (error) {
                    console.log(error);
                } else {
                    return res.status(200).json({mensaje: "Medios Ok", data: medio});
                }
            });
        }
    });
}

module.exports = {
    newmedio,
    getmedios,
    getmedio
};