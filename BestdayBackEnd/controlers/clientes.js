/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const Cliente = require('../models/clienteModel');
const Trimestre = require('../models/trimestreModel');

function newCliente(req, res) {
    data = req.body;
    var cliente = new Cliente();
    console.log(data);
    console.log(data.name);
    cliente.imagen = data.imagen;
    cliente.fecha = new Date();
    cliente.name = "" + req.body.name;
    cliente.status='bueno'
    console.log(cliente);

    cliente.save(function (error, cliente) {
        if (error) {
            console.log("Error al salvar el nuevo cliente: " + error + "\n");
            return res.status(500).json({mensaje: "Error al salvar el nuevo cliente: ", error: error});
        }
        else {
            return res.status(200).json({mensaje: "Registro Cliente OK", data: cliente});
        }
    });
}

function getClientes(req, res) {
    Cliente.find({})
        .populate({path: "trimestre"})
        .exec(function (error,clientes) {
            if (error) {
                console.log('Tenemos algun erro al buscar los clientes');
                return res.status(500).json({mensaje: 'TTenemos algun erro al buscar los clientes',error});
            } else {
                return res.status(200).json({mensaje: "Canales Ok", data:clientes});
            }
        });
}

function getCliente(req, res) {
}

function deleteCliente(req, res) {
}

function updateCliente(req, res) {
}

module.exports = {
    newCliente,
    getCliente,
    getClientes,
    deleteCliente,
    updateCliente

}