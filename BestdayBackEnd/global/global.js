/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
exports.GLOBAL = {
    //Codigos de Mensaje de Error
    M501:'Faltan datos',
    M502:'Usuario No Encontrado',
    M503:'Usuario No Permitido',
    M504:'Datos No Coinciden',
    //Codigos Mensaje Confirmacion
    M200:"User Acetp"
}