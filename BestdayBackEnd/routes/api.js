/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
var express = require('express');
var router = express.Router();
var controladores = require('.././controlers');
var md_auth= require('../middlewares/authenticated');

/* Metodos GET API REST. */
router.get('/role/getroles',md_auth.ensureAuth,controladores.roles.getRoles);
router.get('/user/getuser/:userid',md_auth.ensureAuth, controladores.users.getUser);
router.get('/user/getusers',md_auth.ensureAuth, controladores.users.getUsers);
router.get('/medio/getmedio/:idmedio',md_auth.ensureAuth, controladores.medios.getmedio);
router.get('/medio/getmedios',md_auth.ensureAuth, controladores.medios.getmedios);
router.get('/canal/getcanales',md_auth.ensureAuth, controladores.canales.getcanales);
router.get('/puesto/getpuestos',md_auth.ensureAuth, controladores.puesto.getPuestos);
router.get('/puesto/getpuesto/:idpuesto',md_auth.ensureAuth, controladores.puesto.getPuesto);
router.get('/cliente/clientes',md_auth.ensureAuth, controladores.clientes.getClientes);
router.get('/proyecto/proyectos',md_auth.ensureAuth, controladores.proyectos.getProyectos);
router.get('/proyecto/getproyecto/:idproyecto',md_auth.ensureAuth, controladores.proyectos.getProyecto);
router.get('/trimestre/gettrimestre/:idtrimestre',md_auth.ensureAuth, controladores.trimestre.getTrimestre);
router.get('/trimestre/gettrimestres/',md_auth.ensureAuth, controladores.trimestre.getTrimestres);
router.get('/trimestre/gettrimestresbyproyect/:idproyecto',md_auth.ensureAuth, controladores.trimestre.getTrimestresByProyecto);
/* Metodos POST API REST. */
router.post('/login', controladores.login.login);
router.post('/user/newuser',md_auth.ensureAuth, controladores.users.newUser);
router.post('/medio/newmedio',md_auth.ensureAuth, controladores.medios.newmedio);
router.post('/roles/new',md_auth.ensureAuth,controladores.roles.newRole);
router.post('/canal/newcanal',md_auth.ensureAuth,controladores.canales.newcanal);
router.post('/puesto/newpuesto',md_auth.ensureAuth, controladores.puesto.newPuesto);
router.post('/trimestre/newtrimestre',md_auth.ensureAuth, controladores.trimestre.newTrimestre);
router.post('/proyecto/newproyecto',md_auth.ensureAuth, controladores.proyectos.newProyecto);
//router.post('/newuser', controladores.users.newUser);//Crea Usuarios sin Requerir Token
router.post('/cliente/newcliente',md_auth.ensureAuth,controladores.clientes.newCliente);
/* Metodos PUT API REST. */
router.put('/user/putuser/:userid',md_auth.ensureAuth, controladores.users.updateUser);
/* Metodos DELET API REST. */
router.delete('/user/deletuser/:userid',md_auth.ensureAuth, controladores.users.deleteUser);


module.exports = router;