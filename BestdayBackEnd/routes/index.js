/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
