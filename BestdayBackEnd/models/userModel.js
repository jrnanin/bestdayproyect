/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;
//Nota segun yo puesto y role son lo mismo
const UserSchema = Schema({
    "name": {type: String},
    "apaterno": {type: String},
    "amaterno": {type: String},
    "puesto": {type: Schema.Types.ObjectId, ref: "puesto"},
    "role":   {type: Schema.Types.ObjectId, ref: "role"},
    "canal": [{type: Schema.Types.ObjectId, ref: "canal"}],
    "medio": [{type: Schema.Types.ObjectId, ref: "medio"}],
    "correo": {type: String, lowercase: true, index: {unique: true}},
    "password": {type: String},
    "dateLogin": {type: Date},
    "dateCreate": {type: Date},
    "status": {type: Boolean, enum: [true, false]},
    "delete": {type: Boolean, enum: [true, false]}


});
UserSchema.plugin(AutoIncrement, {inc_field: 'idUser'});
module.exports = mongoose.model('user', UserSchema);

//"medio":{ type: { type: Schema.ObjectId, ref: "medio" }},