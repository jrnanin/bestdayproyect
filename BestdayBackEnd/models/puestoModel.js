/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const PuestoSchema = Schema({
    "name":{ type: String,index: { unique: true }},
    "delete":{ type: Boolean, enum:[true,false] }
});
PuestoSchema.plugin(AutoIncrement, {inc_field: 'idPuesto'});
module.exports = mongoose.model('puesto', PuestoSchema );