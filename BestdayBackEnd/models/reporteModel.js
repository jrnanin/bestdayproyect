/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;
const ReportesSchema=Schema({
    "name": {type:String},
    "medio":{type: Schema.Types.ObjectId, ref: "medio"},
    "archivo":{type:String},
    "fechaCreacion":{type:Date}
//Ver el usuario asignado al medio para que solo se pueda ver la lista del medio
});
ReportesSchema.plugin(AutoIncrement, {inc_field: 'idReporte'});
module.exports = mongoose.model('reporte', ReportesSchema );