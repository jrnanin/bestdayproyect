/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const MediosSchema = Schema({
    "name":{ type: String,index: { unique: true }},
    "canal":{ type: String },
    "delete":{ type: Boolean, enum:[true,false] }


});
MediosSchema.plugin(AutoIncrement, {inc_field: 'idMedio'});
module.exports = mongoose.model('medio', MediosSchema );