/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;
const ClienteSchema=Schema({
    "name": {type:String},
    "imagen":{type: String},
    "fechaAlta":{type: Date},
    "proyectos":[{type: Schema.Types.ObjectId,ref: "proyecto"}]
    //"status":{type: String ,enum :['bueno','regular','malo']}


});
ClienteSchema.plugin(AutoIncrement, {inc_field: 'idCliente'});
module.exports = mongoose.model('cliente', ClienteSchema );