/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const ProyectoSchema = Schema({
    "name":{ type: String , index: { unique: true }},
    "anunciante":{ type: String },
    "fechaInicio": { type: String },
    "fechaFinalizado":{ type: String },
    "status":{type: String ,enum :['activo','finalizado','cancelado','iniciado']},
    "trimestre":[{type: Schema.Types.ObjectId,ref: "trimestre"}],
    "delete":{ type: Boolean, enum:[true,false] },
    "imagen":{ type: String },
    "medio":[{type: Schema.Types.ObjectId, ref: "medio"}]
//Fata ver si se asingan un usuario en especifico para que pueda ver este proyecto

});
ProyectoSchema .plugin(AutoIncrement, {inc_field: 'idProyecto'});
module.exports = mongoose.model('proyecto', ProyectoSchema );