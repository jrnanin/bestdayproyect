/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const RoleSchema = Schema({
    "name":{ type: String, index: { unique: true } },
    "delete":{ type: Boolean, enum:[true,false] }


});
RoleSchema.plugin(AutoIncrement, {inc_field: 'idRole'});
module.exports = mongoose.model('role', RoleSchema );