/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;
const TrimestreSchema=Schema({
    "name": {type:String},
    "fechaCreacion":{type:Date},
    "fechaInicio":{type: Date},
    "fechaFinal":{type: Date},
    "medio": [{type: Schema.Types.ObjectId, ref: "medio"}],
    "reportes":[{type: Schema.Types.ObjectId, ref: "reporte"}],
    "delete":{ type: Boolean, enum:[true,false] },
    "proyecto":{type: Schema.Types.ObjectId, ref: "proyecto"}
//Ver el usuario asignado al medio para que solo se pueda ver la lista del medio
});
TrimestreSchema.plugin(AutoIncrement, {inc_field: 'idTrimestre'});
module.exports = mongoose.model('trimestre', TrimestreSchema );