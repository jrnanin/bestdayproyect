/**
 * Creado por Fernando Hernandez
 * Correo jrnanin@gmail.com
 */
'use strict'
const mongoose= require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const CanalSchema = Schema({
    "name":{ type: String, index: { unique: true }},
    "delete":{ type: Boolean, enum:[true,false] }


});
CanalSchema.plugin(AutoIncrement, {inc_field: 'idCanal'});
module.exports = mongoose.model('canal', CanalSchema );