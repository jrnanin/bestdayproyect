import {Injectable} from '@angular/core';
import {GLOBALURL} from "../globals/global.Urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Canal} from "../models/canal.model";

@Injectable()
export class CanalesService {

  public url: string;

  constructor(public _http: HttpClient) {
    this.url = GLOBALURL.url;
  }

  getCanales() {
    var urlComplemento:string='canal/getcanales';
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.get(this.url + urlComplemento, {headers});
  }
  saveCanal(canal:Canal){
    var urlComplemento:string='canal/newcanal';
    let headers = new HttpHeaders(
      {'Content-Type':'application/json',
        'Authorization': localStorage.getItem('token')}
    );
    console.log(this.url + urlComplemento);
    return this._http.post(
      this.url + urlComplemento,
      JSON.stringify(canal),
      {headers});
  }

}
