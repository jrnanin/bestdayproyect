import {Injectable} from '@angular/core';
import {GLOBALURL} from "../globals/global.Urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class ProyectosService {

  public url: string;

  constructor(private _http: HttpClient) {
    this.url = GLOBALURL.url;
  }

  saveProyecto(newProyecto) {
    var urlComplemento: string = 'proyecto/newproyecto';
    var obj = JSON.stringify(newProyecto);
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.post(this.url + urlComplemento, obj, {headers});

  }

  getProyectos() {
    var urlComplemento: string = 'proyecto/proyectos';

    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.get(this.url + urlComplemento,{headers});
  }

  getProyecto(idProyecto) {
    var urlComplemento: string = 'proyecto/getproyecto/'+idProyecto;

    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.get(this.url + urlComplemento,{headers});
  }

  updatePoryecto() {
  }

  deleteProyecto() {
  }
}

