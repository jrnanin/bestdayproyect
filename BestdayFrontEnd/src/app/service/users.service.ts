import { Injectable } from '@angular/core';
import {GLOBALURL} from "../globals/global.Urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../models/user.model";

@Injectable()
export class UsersService {

  public url: string;

  constructor(public _http: HttpClient) {
    this.url = GLOBALURL.url;
  }

  getUser(iduser:string) {
    console.log(iduser);
    var urlconsulta ='user/getuser/'+iduser;
    let headers = new HttpHeaders(
      {'Content-Type':'application/json',
        'Authorization': localStorage.getItem('token')}
    );
    console.log(this.url + urlconsulta);
    return this._http.get(this.url + urlconsulta, {headers});
  }
  editUser(user:User){
    var urlconsulta='user/update';
    let headers = new HttpHeaders(
      {'Content-Type':'application/json',
        'Authorization': localStorage.getItem('token')}
    );
    console.log(this.url + urlconsulta);
    return this._http.put(this.url + urlconsulta,JSON.stringify(user), {headers});
  }
  getUsers() {
    let headers = new HttpHeaders(
      {'Content-Type':'application/json',
        'Authorization': localStorage.getItem('token')}
    );
    console.log(this.url + 'user/getusers');
    return this._http.get(this.url + 'user/getusers', {headers});
  }
  newUser(newUser:User){
    let headers = new HttpHeaders(
      {'Content-Type':'application/json',
        'Authorization': localStorage.getItem('token')}
    );
    console.log(this.url + 'user/newuser');
    return this._http.post(this.url + 'user/newuser',JSON.stringify(newUser), {headers});
  }
}
