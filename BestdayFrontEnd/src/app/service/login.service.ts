import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import 'rxjs/Rx'
import {GLOBALURL} from '../globals/global.Urls'
import {User} from "../models/user.model";

@Injectable()
export class LoginService {

  public url: string;


  constructor(public _http: HttpClient) {
    console.log("Servicio Listo");
    this.url = GLOBALURL.url;
  }

  singup(user:User) {
    console.log('ObteniendoLogin');
    return this._http.post(this.url + 'login', user);
  }
}

