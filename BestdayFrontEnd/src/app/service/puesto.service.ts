import {Injectable} from '@angular/core';
import {GLOBALURL} from "../globals/global.Urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Puesto} from "../models/puesto.model";

@Injectable()
export class PuestoService {

  public url: string;

  constructor(public _http: HttpClient) {
    this.url = GLOBALURL.url;
  }

  getPuestos() {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + 'puesto/getpuestos');
    return this._http.get(this.url + 'puesto/getpuestos', {headers});
  }

  newPuesto(newPuesto: Puesto) {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + 'puesto/newPuesto');
    return this._http.post(this.url + 'puesto/newPuesto', JSON.stringify(newPuesto), {headers});
  }

  getPuesto() {
  }

  deletePuesto() {
  }

  updatePuesto() {
  }

}
