import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GLOBALURL} from "../globals/global.Urls";

@Injectable()
export class ClientesService {

  public url: string;

  constructor(private _http: HttpClient) {
    this.url = GLOBALURL.url;
  }

  saveCliente(newCliente) {
    var urlComplemento: string = 'cliente/newcliente';
    var obj = JSON.stringify(newCliente);
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.post(this.url + urlComplemento,obj,{headers});
  }
  getClientes(){
    var urlComplemento: string = 'cliente/clientes';

    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.get(this.url + urlComplemento,{headers});
  }
  getCliente(){}
}
