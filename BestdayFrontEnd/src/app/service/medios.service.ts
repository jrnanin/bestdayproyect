import {Injectable} from '@angular/core';
import {GLOBALURL} from "../globals/global.Urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Medio} from "../models/medio.model";

@Injectable()
export class MediosService {

  public url: string;

  constructor(public _http: HttpClient) {
    this.url = GLOBALURL.url;
  }

  getMedio() {
  }

  getMedios() {
    var urlComplemento:string='medio/getmedios';
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    return this._http.get(this.url + urlComplemento, {headers});
  }

  saveMedio(medio:Medio){
    var urlComplemento:string='medio/newmedio';
    let headers = new HttpHeaders(
      {'Content-Type':'application/json',
        'Authorization': localStorage.getItem('token')}
    );
    console.log(this.url + urlComplemento);
    return this._http.post(
      this.url + urlComplemento,
      JSON.stringify(medio),
      {headers});
  }
}
