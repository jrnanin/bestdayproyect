import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GLOBALURL} from "../globals/global.Urls";

@Injectable()
export class TrimestreService {

  public url: string;

  constructor(private _http: HttpClient) {
    this.url = GLOBALURL.url;
  }
  newTrimestre(idproyecto){
    var urlComplemento: string = 'trimestre/newtrimestre';
    var obj = JSON.stringify({proyecto:idproyecto});
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + urlComplemento);
    //console.log(JSON.stringify(obj));
    return this._http.post(this.url + urlComplemento, obj, {headers});
  }
  getTrimestre(){}
  updateTrimestre(){}

}
