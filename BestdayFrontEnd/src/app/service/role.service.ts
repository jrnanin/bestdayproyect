import { Injectable } from '@angular/core';
import {GLOBALURL} from "../globals/global.Urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class RoleService {

  public url:string;
  constructor(public _http: HttpClient) {
    this.url = GLOBALURL.url;
  }
  getRoles() {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token')
      }
    );
    console.log(this.url + 'role/getroles');
    return this._http.get(this.url + 'role/getroles', {headers});
  }
}
