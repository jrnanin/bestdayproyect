import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from './models/user.model';
import {LoginService} from "./service/login.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  public title = 'GestorMEdios'
  public loginActiv = true;


  constructor(private _router: Router) {
  }

  ngOnInit() {
    var Varlogin: string = localStorage.getItem('loginActiv');
    if (Varlogin != null) {
      console.log('existe la variable en el componente principal');
      console.log(JSON.parse(Varlogin).loginActiv);
      this.loginActiv = JSON.parse(Varlogin).loginActiv;
      if (this.loginActiv == false) {
        this._router.navigate(['/home']);
      } else if (this.loginActiv == true) {
        this._router.navigate(['/login']);
      }
      else {
        console.log('no existe la variable en el componente principal');
        this.loginActiv = true;
        localStorage.setItem('loginActiv', JSON.stringify({'loginActiv': true}));
        this._router.navigate(['/login']);
      }

    }
  }
}

