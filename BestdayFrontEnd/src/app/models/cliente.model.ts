
import {Medio} from "./medio.model";
import {Trimestre} from "./trimestre.model";

export class Cliente{
  constructor(
    public _id: string,
    public name: string,
    public imagen: string,
    public fecha: string,
    public status: string,
    public medios:Medio[],
    public triemestres:Trimestre[]){
  };
};
