export class User {
  constructor(public name: string,
              public apaterno: string,
              public amaterno: string,
              public puesto: string,
              public canal: string,
              public medio: string,
              public correo: string,
              public password: string,
              public role: string,
              public status: boolean,
              public idUser: number) {
  };

};
