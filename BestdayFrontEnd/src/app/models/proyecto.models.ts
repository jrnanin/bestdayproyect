import {Trimestre} from "./trimestre.model";

export class Proyecto {
  public _id: string;
  public name: string;
  public anunciante: string;
  public imagen: string;
  public fechaInicio: Date;
  public fechaFinalizado: Date;
  public status: string;
  public trimestre: Trimestre[];

  //constructor();

  constructor(_id: string,
              name: string,
              anunciante: string,
              imagen: string,
              fechaInicio: Date,
              fechaFinalizado: Date,
              status: string,
              trimestre: Trimestre[]) {
    this._id = _id;
    this.name = name;
    this.anunciante = anunciante;
    this.imagen = imagen;
    this.fechaInicio = fechaInicio;
    this.fechaFinalizado = fechaFinalizado;
    this.status = status;
    this.trimestre = trimestre;

  }

}
