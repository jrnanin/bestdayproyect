import {Canal} from '../models/canal.model'
export class Medio{
  constructor(
    public _id: string,
    public idMedio: string,
    public name: string,
    public canal: Canal){
  };
};
