import {RouterModule, Routes} from '@angular/router';
//import { } from './components/';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {UsuariosComponent} from './components/usuarios/usuarios/usuarios.component';
import {NuevoUsuarioComponent} from './components/usuarios/nuevo-usuario/nuevo-usuario.component';
import {MediosComponent} from './components/medios/medios.component';
import {NuevoMedioComponent} from './components/medios/nuevo-medio.component';
import {ProyectosComponent} from './components/proyectos/lista-proyectos/proyectos.component';
import {NuevoProyectoComponent} from './components/proyectos/nuevo-Proyecto/nuevo-proyecto.component';
import {GetRolesComponent} from './components/role/get-roles/get-roles.component';
import {NewRoleComponent} from './components/role/new-role/new-role.component';
import {NewCanalComponent} from './components/canales/new-canal/new-canal.component'
import {GetCanalesComponent} from './components/canales/get-canales/get-canales.component'
import {GetPuestosComponent} from './components/puestos/get-puestos/get-puestos.component';
import {NewPuestoComponent} from './components/puestos/new-puesto/new-puesto.component';
import {ClientesComponent} from './components/cliente/clientes/clientes.component';
import {NuevoClienteComponent} from './components/cliente/nuevo-cliente/nuevo-cliente.component';
import {ProyectoComponent} from './components/proyectos/proyecto/proyecto.component';
import {UserProfileComponent} from './components/usuarios/user-profile/user-profile.component';
import {EditUserComponent} from './components/usuarios/edit-user/edit-user.component';

const APP_ROUTES: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'usuarios', component: UsuariosComponent},
  {path: 'usuario/nuevousuario', component: NuevoUsuarioComponent},
  {path: 'userprofile', component: UserProfileComponent},
  {path: 'edituser/:id', component: EditUserComponent},
  {path: 'deleteuser', component: EditUserComponent},
  {path: 'medios', component: MediosComponent},
  {path: 'medio/nuevomedio', component: NuevoMedioComponent},
  {path: 'proyectos', component: ProyectosComponent},
  {path: 'proyecto/nuevoproyecto', component: NuevoProyectoComponent},
  {path: 'roles', component: GetRolesComponent},
  {path: 'role/nuevorole', component: NewRoleComponent},
  {path: 'canales', component: GetCanalesComponent},
  {path: 'canal/nuevocanal', component: NewCanalComponent},
  {path: 'puestos', component: GetPuestosComponent},
  {path: 'puesto/nuevopuesto', component: NewPuestoComponent},
  {path: 'clientes', component: ClientesComponent},
  {path: 'cliente/nuevocliente', component: NuevoClienteComponent},
  {path: 'proyecto/:id', component: ProyectoComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'login'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
