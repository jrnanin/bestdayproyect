import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';

//Servicios
import {LoginService} from './service/login.service';
import {AuthGuardService} from './service/auth-guard.service';
import {UsersService} from './service/users.service';
import {CanalesService} from "./service/canales.service";
import {PuestoService} from "./service/puesto.service";
import {RoleService} from "./service/role.service";
import {MediosService} from "./service/medios.service";
import {ClientesService} from "./service/clientes.service";
import {ProyectosService} from "./service/proyectos.service";
import {TrimestreService} from "./service/trimestre.service";
//Rutas
import {APP_ROUTING} from './app.routes';
//Componentes
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/shared/navbar/navbar.component';
import {MediosComponent} from './components/medios/medios.component';
import {LoginComponent} from './components/login/login.component';
import {UsuariosComponent} from './components/usuarios/usuarios/usuarios.component';
import {NuevoUsuarioComponent} from './components/usuarios/nuevo-usuario/nuevo-usuario.component';
import {HomeComponent} from './components/home/home.component';
import {NuevoMedioComponent} from './components/medios/nuevo-medio.component';
import {ProyectosComponent} from './components/proyectos/lista-proyectos/proyectos.component';
import {NuevoProyectoComponent} from './components/proyectos/nuevo-Proyecto/nuevo-proyecto.component';
import {NewRoleComponent} from './components/role/new-role/new-role.component';
import {GetRolesComponent} from './components/role/get-roles/get-roles.component';
import {NewCanalComponent} from './components/canales/new-canal/new-canal.component';
import {GetCanalesComponent} from './components/canales/get-canales/get-canales.component';
import {NuevoClienteComponent} from './components/cliente/nuevo-cliente/nuevo-cliente.component';
import {ClientesComponent} from './components/cliente/clientes/clientes.component';
import {GetPuestosComponent} from './components/puestos/get-puestos/get-puestos.component';
import {NewPuestoComponent} from './components/puestos/new-puesto/new-puesto.component';
import {ClienteComponent} from './components/cliente/cliente/cliente.component';
import {ProyectoComponent} from './components/proyectos/proyecto/proyecto.component';
import { UserProfileComponent } from './components/usuarios/user-profile/user-profile.component';
import { EditUserComponent } from './components/usuarios/edit-user/edit-user.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MediosComponent,
    LoginComponent,
    UsuariosComponent,
    NuevoUsuarioComponent,
    HomeComponent,
    NuevoMedioComponent,
    ProyectosComponent,
    NuevoProyectoComponent,
    NewRoleComponent,
    GetRolesComponent,
    NewCanalComponent,
    GetCanalesComponent,
    NuevoClienteComponent,
    ClientesComponent,
    GetPuestosComponent,
    NewPuestoComponent,
    ClienteComponent,
    ProyectoComponent,
    UserProfileComponent,
    EditUserComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    LoginService,
    AuthGuardService,
    UsersService,
    MediosService,
    CanalesService,
    PuestoService,
    RoleService,
    ClientesService,
    ProyectosService,
    TrimestreService

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
