import {Component, OnInit} from '@angular/core';
import {CanalesService} from "../../service/canales.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Canal} from "../../models/canal.model";
import {Medio} from "../../models/medio.model";
import {MediosService} from "../../service/medios.service";

@Component({
  selector: 'app-nuevo-medio',
  templateUrl: './nuevo-medio.component.html',
  styles: []
})
export class NuevoMedioComponent implements OnInit {
  public newMedioform: FormGroup;
  public canales: Canal[] = [];

  constructor(private _canalesService: CanalesService, private _mediosService:MediosService) {
    this.newMedioform = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'selectCanal': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.canales.push(new Canal('0', '0', 'Selecciona una Opción'));
    this._canalesService.getCanales()
      .subscribe((resp: any) => {
        console.log(resp);
        for (var ind = 0; ind < resp.data.length; ind++) {
          console.log(resp.data[ind]);
          this.canales.push(resp.data[ind]);
        }
      }, (err: any) => {
        console.log(err);
      });
  }

  func_newMedio() {
    var newMewdio = new Medio(
      '',
      '',
      ''+this.newMedioform.controls['nombre'].value,
      this.objMedio(this.newMedioform.controls['selectCanal'].value)
    );
    this._mediosService.saveMedio(newMewdio).subscribe(
      (resp:any)=>{
        console.log(resp);
      },
      (error:any)=>{});
  }

  objMedio(idMedio:string){
    for(var ind=0;ind <this.canales.length;ind++){
      if(this.canales[ind]._id==idMedio){
        return this.canales[ind];
      }
    }
    return null;
  }
}
