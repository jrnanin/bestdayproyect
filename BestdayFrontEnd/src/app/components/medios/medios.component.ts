import {Component, OnInit} from '@angular/core';
import {MediosService} from "../../service/medios.service";
import {Medio} from "../../models/medio.model";

@Component({
  selector: 'app-medios',
  templateUrl: './medios.component.html',
  styles: []
})
export class MediosComponent implements OnInit {
  public medios: Medio[] = [];

  constructor(private _medioService: MediosService) {
  }

  ngOnInit() {
    console.log('Cargando lista de Medios');
    this._medioService.getMedios().subscribe((resp: any) => {
      console.log(resp);
      for (var ind = 0; ind < resp.data.length; ind++) {

        this.medios.push(resp.data[ind]);
      }
    }, (error: any) => {
      console.log('Se tiene error para ver los medios')
    })
  }

}
