import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProyectosService} from "../../../service/proyectos.service";
import {Proyecto} from "../../../models/proyecto.models";
import {TrimestreService} from "../../../service/trimestre.service";

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html'
})
export class ProyectoComponent {
  public proyecto: Proyecto =null;
  private idproyecto: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _proyectoService: ProyectosService,
              private _trimestresService: TrimestreService) {
  }

  ngOnInit() {
    //this.proyecto=new Proyecto();
    this._activatedRoute.params.subscribe(params => {
      this.idproyecto = params['id'];
      this._proyectoService.getProyecto(this.idproyecto).subscribe(
        (resp: any) => {
          this.proyecto = resp.data;
          console.log(this.proyecto);
        },
        (error: any) => {
          console.log(error);
        }
      );
    });


  }

  creaTrimestre() {
    this._trimestresService.newTrimestre(this.idproyecto).subscribe(
      (resp:any) => {
        console.log(resp);
        this.proyecto.trimestre.push(resp.data);
      }, (error:any) => {
        console.log(error)
      }
      );
  }


  setMyStyles(status) {

    if (status == 'cancelado') {
      return {'border-color': '#e74c3c'};
    }
    else if (status == 'iniciado') {
      return {'border-color': '#2ecc71'};
    }
    else if (status == 'activo') {
      return {'border-color': '#f7dc6f'};
    }
    else if (status == 'finalizado') {
      return {'border-color': '#00BFFF'};
    }

  }
}
