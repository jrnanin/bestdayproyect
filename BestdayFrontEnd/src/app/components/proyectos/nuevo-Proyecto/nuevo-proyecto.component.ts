import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ProyectosService} from "../../../service/proyectos.service";
import {Proyecto} from "../../../models/proyecto.models";

@Component({
  selector: 'app-nuevo-proyecto',
  templateUrl: './nuevo-proyecto.component.html',
  styles: []
})
export class NuevoProyectoComponent implements OnInit {

  public newProyectoform: FormGroup;
  public imagen:string;

  constructor(private _proyectoService: ProyectosService) {
    this.newProyectoform=new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'anunciante': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  func_newProyecto() {
    var proyecto=new Proyecto(
      '',
      ''+this.newProyectoform.controls['nombre'].value,
      ''+this.newProyectoform.controls['anunciante'].value,
      ''+this.imagen,
      null,
      null,
      '',
      null);
    console.log(proyecto);
    this._proyectoService.saveProyecto(proyecto).subscribe(
      (resp) => {
        console.log(resp);
      },
      (error) => {
        console.log(error);
      });
  }

  onFileChange(event) {
    //console.log(event);
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imagen="data:"+file.type+";base64,"+reader.result.split(',')[1]
      };

    }
  }

}
