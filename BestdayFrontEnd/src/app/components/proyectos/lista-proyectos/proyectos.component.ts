import {Component, OnInit} from '@angular/core';
import {Proyecto} from "../../../models/proyecto.models";
import {ProyectosService} from "../../../service/proyectos.service";

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styles: []
})
export class ProyectosComponent implements OnInit {

  public proyectos: Proyecto[] = [];

  constructor(private _proyectosService: ProyectosService) {

  }

  ngOnInit() {
    this._proyectosService.getProyectos().subscribe(
      (resp: any) => {
        console.log(resp);
        for (var ind = 0; ind < resp.data.length; ind++) {
          this.proyectos.push(resp.data[ind]);
        }
      },
      (error: any) => {
      });
  }


  setMyStyles(status) {

    if (status == 'cancelado') {
      return {'border-color': '#e74c3c'};
    }
    else if (status == 'iniciado') {
      return {'border-color': '#2ecc71'};
    }
    else if (status == 'activo') {
      return {'border-color': '#f7dc6f'};
    }
    else if (status == 'finalizado') {
      return {'border-color': '#00BFFF'};
    }

  }
}
