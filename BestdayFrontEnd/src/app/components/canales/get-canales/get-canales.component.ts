import { Component, OnInit } from '@angular/core';
import {CanalesService} from "../../../service/canales.service";
import {Canal} from "../../../models/canal.model";

@Component({
  selector: 'app-get-canales',
  templateUrl: './get-canales.component.html',
  styleUrls: []
})
export class GetCanalesComponent implements OnInit {

  private canales:Canal[]=[];
  constructor(private  _canalesService:CanalesService) { }

  ngOnInit() {

    this._canalesService.getCanales().subscribe(
      (resp:any)=>{
        console.log(resp);
        for(var ind=0;ind<resp.data.length;ind++){
          this.canales.push(resp.data[ind]);
        }
      },
      (error:any)=>{});

  }

}

