import { Component, OnInit } from '@angular/core';
import {CanalesService} from "../../../service/canales.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Canal} from "../../../models/canal.model";

@Component({
  selector: 'app-new-canal',
  templateUrl: './new-canal.component.html',
  styleUrls: []
})
export class NewCanalComponent implements OnInit {

  private newCanalform: FormGroup;
  constructor(private _canalesService: CanalesService) {
    this.newCanalform=new FormGroup({
      'nombre': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  func_newCanal(){
    var canal = new Canal('',
      '',
      ''+this.newCanalform.controls['nombre'].value);
    this._canalesService.saveCanal(canal).subscribe(
      (resp:any)=>{console.log(resp)},
      ()=>{})
  }

}
