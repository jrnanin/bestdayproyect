import { Component, OnInit } from '@angular/core';
import {User} from "../../../models/user.model";
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  public user:User;
  constructor() {
    this.user=JSON.parse(localStorage.getItem('user'))
  }

  ngOnInit() {
  }

}
