import {Component, OnInit} from '@angular/core';
import {PuestoService} from "../../../service/puesto.service";
import {Puesto} from "../../../models/puesto.model";

@Component({
  selector: 'app-get-puestos',
  templateUrl: './get-puestos.component.html',
  styles: []
})
export class GetPuestosComponent implements OnInit {

  private puestos: Puesto[]=[];
  constructor(private _puestosService: PuestoService) {
  }

  ngOnInit() {
    this._puestosService.getPuestos().subscribe(
      (resp: any) => {
        console.log(resp)
        for(var ind=0;ind<resp.data.length;ind++){
          this.puestos.push(resp.data[ind]);
        }
      },
      (error: any) => {
      });
  }

}
