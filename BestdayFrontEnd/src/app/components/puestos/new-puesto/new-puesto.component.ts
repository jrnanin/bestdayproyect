import {Component, OnInit} from '@angular/core';
import {Puesto} from "../../../models/puesto.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PuestoService} from "../../../service/puesto.service";

@Component({
  selector: 'app-new-puesto',
  templateUrl: './new-puesto.component.html',
  styles: []
})
export class NewPuestoComponent implements OnInit {

  public newPuesto: Puesto;
  newPuestoform: FormGroup;

  constructor(private _puestoService: PuestoService) {
    this.newPuestoform = new FormGroup({
      'nombre': new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
  }

  func_newPuesto() {
    console.log("Creando nuevo Puesto");
    this.newPuesto = new Puesto('', '', '' + this.newPuestoform.controls['nombre'].value);
    this._puestoService.newPuesto(this.newPuesto).subscribe((resp: any) => {
      console.log(resp);
    }, (err: any) => {
      console.log(err);
    });

  }
}
