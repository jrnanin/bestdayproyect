import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {LoginService} from '../../service/login.service';
import {User} from "../../models/user.model";
import {JwtHelper} from 'angular2-jwt'
import {Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public loginActiv = true;
  loginform: FormGroup;
  public user: User;
  public jwtHelper: JwtHelper = new JwtHelper()

  constructor(private _loginService: LoginService, private _router: Router) {
    this.loginform = new FormGroup({
      'correo': new FormControl('', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
      'password': new FormControl('', Validators.required),
    })
  }

  ngOnInit() {
    var Varlogin: string = localStorage.getItem('loginActiv');
    if (Varlogin != null) {
      console.log('existe la variable para cargar la vista Login');
      console.log(JSON.parse(Varlogin).loginActiv);
      this.loginActiv = JSON.parse(Varlogin).loginActiv;
      if (this.loginActiv == false) {
        this._router.navigate(['/home']);
      } else if (this.loginActiv == true) {
        this._router.navigate(['/login']);
      }
    }
    else {
      console.log('no existe la variable en el componente login');
      this.loginActiv = true;
      localStorage.setItem('loginActiv', JSON.stringify({'loginActiv': true}));
      this._router.navigate(['/login']);
    }
  }

  func_login() {
    console.log("logeando");
    this.user = new User('',
      '',
      '',
      '',
      '',
      '',
      this.loginform.controls['correo'].value,
      this.loginform.controls['password'].value,
      '',
      true,
      0);
    //console.log(this.user);
    this._loginService.singup(this.user).subscribe(
      resp => {
        //console.log(resp);
        let datas: any;
        datas = resp;
        localStorage.setItem('token', datas.user_token);
        var token = this.jwtHelper.decodeToken(datas.user_token);
        this.user.name = token.name;
        this.user.apaterno = token.apaterno;
        this.user.amaterno = token.amaterno;
        this.user.role = token.role;
        this.user.puesto=token.puesto;
        this.user.status = token.status;
        this.user.idUser = token.sub;
        localStorage.setItem('user', JSON.stringify(this.user));
        localStorage.setItem('loginActiv', JSON.stringify({'loginActiv': false}));
        //console.log(this.jwtHelper.isTokenExpired(datas.user_token))
        this._router.navigate(['/home']);

      }, error => {
        console.log(error.error);
        //Falta controlar el mandar los errores de error a la vista
      });
  }

}

