import {Component, OnInit} from '@angular/core';
import {RoleService} from "../../../service/role.service";
import {Role} from "../../../models/role.model";

@Component({
  selector: 'app-get-roles',
  templateUrl: './get-roles.component.html',
  styleUrls: []
})
export class GetRolesComponent implements OnInit {

  public roles:Role[]=[];
  constructor(private  _roleService: RoleService) {
  }

  ngOnInit() {
    console.log('Cargando lista de Roles');
    this._roleService.getRoles().subscribe((resp: any) => {
      console.log(resp)
      for(var ind=0;ind<resp.data.length;ind++){
        this.roles.push(resp.data[ind]);
      }
    }, (error: any) => {
    });
  }

}
