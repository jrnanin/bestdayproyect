import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../service/users.service'
import {User} from "../../../models/user.model";

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html'
})
export class UsuariosComponent implements OnInit {

  public users:User[]=[];
  constructor(private _userService:UsersService) { }

  ngOnInit() {
    console.log('Cargando los datos')
    this._userService.getUsers().subscribe((resp:any)=>{
      console.log(resp);
      for(var ind =0;ind<resp.data.length;ind++){
        this.users.push(resp.data[ind]);
      }

    },error=>{
      console.log(error)
    });
  }


}
