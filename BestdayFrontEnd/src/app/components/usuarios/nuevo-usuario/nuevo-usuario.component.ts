import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {UsersService} from "../../../service/users.service";
import {User} from "../../../models/user.model";
import {Medio} from "../../../models/medio.model";
import {MediosService} from "../../../service/medios.service";
import {CanalesService} from "../../../service/canales.service";
import {PuestoService} from "../../../service/puesto.service";
import {RoleService} from "../../../service/role.service";
import {Canal} from "../../../models/canal.model";
import {Role} from "../../../models/role.model";
import {Puesto} from "../../../models/puesto.model";

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html'
})
export class NuevoUsuarioComponent implements OnInit {

  public newUser: User;
  public medios: Medio[] = [];
  public mediosLista: Medio[] = [];
  public canales: Canal[] = [];
  public canalesLista: Canal[] = [];
  public roles: Role[] = [];
  public puestos: Puesto[] = [];
  newUserform: FormGroup;

  constructor(private _userService: UsersService,
              private _mediosservice: MediosService,
              private _canalesService: CanalesService,
              private _puestoService: PuestoService,
              private _roleService: RoleService) {
    this.newUserform = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'apaterno': new FormControl('', Validators.required),
      'amaterno': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required),
      'listMedio': new FormControl('', Validators.required),
      'selectMedio': new FormControl('', Validators.required),
      'listCanal': new FormControl('', Validators.required),
      'selectCanal': new FormControl('', Validators.required),
      'selectPuesto': new FormControl('', Validators.required),
      'selectRole': new FormControl('', Validators.required)


    })
  }

  ngOnInit() {
    var medio = new Medio('0', '0', 'Selecciona una Opción', null);
    this.medios.push(medio);
    this._mediosservice.getMedios()
      .subscribe((resp: any) => {
        console.log(resp);
        for (var ind = 0; ind < resp.data.length; ind++) {
          this.medios.push(resp.data[ind]);
        }
      }, (err: any) => {
        console.log(err);
      });
    var canal = new Canal('0', '0', 'Selecciona una Opción');
    this.canales.push(canal);
    this._canalesService.getCanales()
      .subscribe((resp: any) => {
        console.log(resp);
        for (var ind = 0; ind < resp.data.length; ind++) {
          this.canales.push(resp.data[ind]);
        }
      }, (err: any) => {
        console.log(err);
      });


    this.roles.push(new Role('0','0','Selecciona una Opción'));
    this._roleService.getRoles()
      .subscribe((resp: any) => {
        console.log(resp);
        for (var ind = 0; ind < resp.data.length; ind++) {
          this.roles.push(resp.data[ind]);
        }
      }, (err: any) => {
        console.log(err);
      });

    this.puestos.push(new Puesto('0','0','Selecciona una Opción'));
    this._puestoService.getPuestos()
      .subscribe((resp: any) => {
        console.log(resp);
        for (var ind = 0; ind < resp.data.length; ind++) {
          this.puestos.push(resp.data[ind]);
        }
      }, (err: any) => {
        console.log(err);
      });
  }

  addMedio() {
    console.log('se movio para agarrar el tag');
    var idmedio = this.newUserform.controls['selectMedio'].value;
    console.log(idmedio);
    var json = [];
    if ((this.newUserform.controls['listMedio'].value).length > 0) {
      json = JSON.parse(this.newUserform.controls['listMedio'].value);
      json.push(idmedio);
      this.mediosLista.push(new Medio(idmedio, '0', '' + this.retornaNombre(idmedio, this.medios), null));
      this.newUserform.controls['listMedio'].setValue(JSON.stringify(json));
    } else {
      console.log('no se tiene datos');
      json.push(idmedio);
      this.mediosLista.push(new Medio(idmedio, '0', '' + this.retornaNombre(idmedio, this.medios), null));
      this.newUserform.controls['listMedio'].setValue(JSON.stringify(json));
    }
  }

  addCanal() {
    console.log('se movio para agarrar el tag');
    var idcanal = this.newUserform.controls['selectCanal'].value;
    console.log(idcanal);
    var json = [];
    if ((this.newUserform.controls['listCanal'].value).length > 0) {
      json = JSON.parse(this.newUserform.controls['listCanal'].value);
      json.push(idcanal);
      this.canalesLista.push(new Canal(idcanal, '0', '' + this.retornaNombre(idcanal, this.canales)));
      this.newUserform.controls['listCanal'].setValue(JSON.stringify(json));
    } else {
      console.log('no se tiene datos');
      json.push(idcanal);
      this.canalesLista.push(new Canal(idcanal, '0', '' + this.retornaNombre(idcanal, this.canales)));
      this.newUserform.controls['listCanal'].setValue(JSON.stringify(json));
    }
  }

  retornaNombre(idmedio: string, arreglo: any[]) {
    for (var ind = 0; ind < arreglo.length; ind++) {
      if (arreglo[ind]._id == idmedio) {
        return arreglo[ind].name;
      }
    }
    return 'No encontrado'
  }

  removeMedio(idmedio: string) {
    var json = [];
    console.log('Removiendo datos');
    json = JSON.parse(this.newUserform.controls['listMedio'].value);
    this.removeElemento(idmedio, json);
    this.newUserform.controls['listMedio'].setValue(JSON.stringify(json));
    this.removejson(idmedio, this.mediosLista);
  }

  removeCanal(idmedio: string) {
    var json = [];
    console.log('Removiendo datos');
    json = JSON.parse(this.newUserform.controls['listCanal'].value);
    this.removeElemento(idmedio, json);
    this.newUserform.controls['listCanal'].setValue(JSON.stringify(json));
    this.removejson(idmedio, this.canalesLista);
  }

  removejson(elemeto: string, arreglo: any[]) {
    for (var ind = 0; ind < arreglo.length; ind++) {
      //console.log(arreglo[ind]);
      if (elemeto == arreglo[ind]._id) {
        arreglo.splice(ind, 1);
      }
    }
  }

  removeElemento(elemeto: string, arreglo: string[]) {
    for (var ind = 0; ind < arreglo.length; ind++) {
      //console.log(arreglo[ind]);
      if (elemeto == arreglo[ind]) {
        arreglo.splice(ind, 1);
      }
    }
  }

  func_newUser() {
    console.log("Cargando usuario Nuevo");
    this.newUser = new User(
      '' + this.newUserform.controls['nombre'].value,
      '' + this.newUserform.controls['apaterno'].value,
      '' + this.newUserform.controls['amaterno'].value,
      '[\"'+this.newUserform.controls['selectPuesto'].value+'\"]',
      '' + this.newUserform.controls['listCanal'].value,
      '' + this.newUserform.controls['listMedio'].value,
      '' + this.newUserform.controls['email'].value,
      '' + this.newUserform.controls['password'].value,
      '[\"'+this.newUserform.controls['selectRole'].value+'\"]',
      true,
      0);
    console.log('Datos nuevo usuario');

    console.log(this.newUser)
    this._userService.newUser(this.newUser).subscribe((resp: any) => {
      console.log(resp);
    }, (err: any) => {
      console.log(err);
    });

  }
}
