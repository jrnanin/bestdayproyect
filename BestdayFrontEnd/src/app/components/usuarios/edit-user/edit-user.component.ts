import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UsersService} from '../../../service/users.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html'
})
export class EditUserComponent implements OnInit {
  public idUsuario:string = null;
  public response:any;
  constructor(private _activatedRoute: ActivatedRoute, private _userService :UsersService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      this.idUsuario = params['id'];
      this._userService.getUser(this.idUsuario).subscribe((resp:any)=>{
        this.response=resp;
        console.log(this.response);
      },(error:any)=>{
        console.log(error);
      });
    });
  }

}
