import {Component, OnInit} from '@angular/core';
import {Cliente} from '../../../models/cliente.model'
import {ClientesService} from "../../../service/clientes.service";

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: []
})
export class ClientesComponent implements OnInit {

  public clientes: Cliente[] = [];

  constructor(private _clientesService: ClientesService) {
  }

  ngOnInit() {
    this._clientesService.getClientes().subscribe(
      (resp: any) => {
        //console.log(resp);
        var data = resp.data;
        for (var ind = 0; ind < data.length; ind++) {
          this.clientes.push(data[ind]);
        }
      },
      () => {
      });
  }

  //style="border-color: #2dcc70"
  setMyStyles(status) {

    if (status == 'malo') {
      return {'border-color': '#e74c3c'};
    }
    else if (status == 'bueno') {
      return {'border-color': ' #2ecc71'};
    }
    else if (status == 'regular') {
      return {'border-color': ' #f7dc6f '};
    }


  }

}
