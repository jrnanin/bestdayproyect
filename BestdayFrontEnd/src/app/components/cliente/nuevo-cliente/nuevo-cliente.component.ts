import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Cliente} from "../../../models/cliente.model";
import {ClientesService} from "../../../service/clientes.service";


@Component({
  selector: 'app-nuevo-cliente',
  templateUrl: './nuevo-cliente.component.html',
  styleUrls: []
})
export class NuevoClienteComponent implements OnInit {

  newClienteform:FormGroup;
  constructor(private _clitenteService:ClientesService) {
    this.newClienteform = new FormGroup({
      'nombre': new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
  }

  func_newCliente(){
    console.log("Creando Nuevo Cliente");
    var cliente = new Cliente(
      '',
      ''+this.newClienteform.controls['nombre'].value,
      null,
      null,'',null,null);
    this._clitenteService.saveCliente(cliente).subscribe(
      (resp: any) => {
        console.log(resp);
    }, (error: any) => {
    });

  }

}
